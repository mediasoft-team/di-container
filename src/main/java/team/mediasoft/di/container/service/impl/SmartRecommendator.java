package team.mediasoft.di.container.service.impl;

public class SmartRecommendator implements Recommendator {
    @Override
    public void recommend() {
        System.out.println("Smart recommendation is in progress");
    }
}
