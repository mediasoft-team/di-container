package team.mediasoft.di.container.service;

import team.mediasoft.di.container.annotation.Inject;
import team.mediasoft.di.container.annotation.PostConstruct;
import team.mediasoft.di.container.model.Gift;
import team.mediasoft.di.container.model.Person;

public class GiftPresenter {

    @Inject
    private GiftChooseHelper giftChooseHelper;

    @Inject
    private PaymentSystem paymentSystem;

    @Inject
    private DeliverySystem deliverySystem;

    @PostConstruct
    public void postConstruct() {
        System.out.println("Gift presenter has been initialized!");
    }

    public void present(Person person) {
        Gift gift = giftChooseHelper.choose(person);
        System.out.println(String.format("Gift has been chosen: %s", gift.getName()));
        paymentSystem.pay(gift);
        deliverySystem.deliver(gift, person);
    }
}
