package team.mediasoft.di.container.service.impl;

import team.mediasoft.di.container.annotation.Inject;
import team.mediasoft.di.container.model.Gift;
import team.mediasoft.di.container.model.Person;
import team.mediasoft.di.container.service.GiftChooseHelper;

public class SmartGiftChooseHelper implements GiftChooseHelper {

    @Inject
    private Recommendator recommendator;

    @Override
    public Gift choose(Person person) {
        recommendator.recommend();
        return new Gift("Smart watches", 10000);
    }
}
