package team.mediasoft.di.container.service.impl;

public interface Recommendator {
    void recommend();
}
