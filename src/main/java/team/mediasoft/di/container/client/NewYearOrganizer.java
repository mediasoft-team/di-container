package team.mediasoft.di.container.client;

import team.mediasoft.di.container.annotation.Inject;
import team.mediasoft.di.container.model.Person;
import team.mediasoft.di.container.service.GiftPresenter;

public class NewYearOrganizer {

    @Inject
    private GiftPresenter giftPresenter;

    public void prepareToCelebration() {
        Person friend = new Person("Иван Иванов");
        giftPresenter.present(friend);
    }
}
