package team.mediasoft.di.container.postprocessor;

public interface BeanPostProcessor {

    void process(Object bean);
}
