package team.mediasoft.di.container.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class Gift {

    private final String name;
    private final Integer price;
}
